﻿Shader "Unlit/VertexColor"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_RampTex("RampTex", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "LightMode"="ForwardBase"}
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			#include "UnityLightingCommon.cginc"	

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 color : COLOR;
				float3 normal : NORMAL;
			};

			sampler2D _MainTex;
			sampler2D _RampTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.color = v.color;
				o.normal = UnityObjectToWorldNormal(v.normal);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float3 lightDir = _WorldSpaceLightPos0.xyz;
				float NdotL = (dot(i.normal, lightDir) + 1) * 0.5;
				float3 lightColor = _LightColor0.rgb;
				float3 ramp = tex2D(_RampTex, float2(NdotL, 0.5)).x * lightColor;
				// sample the texture
				//fixed4 col = tex2D(_MainTex, i.uv);
				return float4(i.color.xyz * ramp, 1);
			}
			ENDCG
		}
	}
}
