﻿Shader "Unlit/TextureRamp"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_RampTex("RampTex", 2D) = "white" {}
		_Color("Color", Color) = (0, 0, 0, 0)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "LightMode"="ForwardBase"}
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			#include "UnityLightingCommon.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float3 normal : NORMAL;
			};

			sampler2D _MainTex;
			sampler2D _RampTex;
			float4 _MainTex_ST;
			float4 _Color;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.normal = UnityObjectToWorldNormal(v.normal);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float3 lightDir = _WorldSpaceLightPos0.xyz;
				float NdotL = (dot(i.normal, lightDir) + 1) * 0.5;
				NdotL = pow(NdotL, 0.2);
				float3 lightColor = _LightColor0.rgb * _Color.rgb;
				float3 ramp = tex2D(_RampTex, float2(NdotL, 0.5)).x * lightColor;	
				fixed4 col = tex2D(_MainTex, i.uv);
				return float4(col * ramp, 1);
			}
			ENDCG
		}
	}
}
