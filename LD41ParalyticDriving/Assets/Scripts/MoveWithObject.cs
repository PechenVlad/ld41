﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWithObject : MonoBehaviour {

    [SerializeField]
    Transform TargetTransform;

    private void Awake()
    {
        StartCoroutine(FUpdate());
    }

    private IEnumerator FUpdate()
    {
        while(true)
        {
            transform.position = TargetTransform.position;
            transform.rotation = TargetTransform.rotation;
            yield return new WaitForFixedUpdate();
        }
    }
}
