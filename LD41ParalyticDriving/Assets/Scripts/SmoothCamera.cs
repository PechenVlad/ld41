﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothCamera : MonoBehaviour {

    [SerializeField]
    Transform HeadTransform;


    private void Update()
    {
        transform.position = Vector3.Lerp(transform.position, HeadTransform.position, Time.deltaTime * 3);
        transform.rotation = Quaternion.Slerp(transform.rotation, HeadTransform.rotation, Time.deltaTime * 3);
    }
}
