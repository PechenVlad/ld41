﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeIn : MonoBehaviour {

    [SerializeField]
    Image Image;

    [SerializeField]
    Text text;
    [SerializeField]
    Text Hint;

    [SerializeField]
    GameObject quest;
    [SerializeField]
    GameObject time;



    void Start()
    {
        quest.SetActive(false);
        time.SetActive(false);
        Image.color = Color.black;
        text.color = Color.white;
    }

    public void StartFadeIn(string text)
    {
        if (fadeIn)
            return;
        fadeIn = true;
        this.text.text = text + "\n(Press Enter to restart)";
        StartCoroutine(FadeInCoroutine());
    }
    bool fadeIn;


    public void StartFadeOut(string text)
    {
        this.text.text = text + "\n(Press Enter to begin)";
        StartCoroutine(FadeOutCoroutine());
    }
    

    IEnumerator FadeInCoroutine()
    {
        float t = 0;

        while (t < 1f)
        {
            t += Time.deltaTime;
            t = Mathf.Clamp01(t);

            Image.color = new Color(Image.color.r, Image.color.g, Image.color.b, t);
            Hint.color = new Color(Hint.color.r, Hint.color.g, Hint.color.b, t);

            text.color = new Color(text.color.r, text.color.g, text.color.b, t);
            yield return null;
        }
    }


    IEnumerator FadeOutCoroutine()
    {
        float t = 1;

        while (t > 0f)
        {
            t -= Time.deltaTime;
            t = Mathf.Clamp01(t);

            Image.color = new Color(Image.color.r, Image.color.g, Image.color.b, t);
            Hint.color = new Color(Hint.color.r, Hint.color.g, Hint.color.b, t);

            text.color = new Color(text.color.r, text.color.g, text.color.b, t);
            yield return null;
        }
        quest.SetActive(true);
        time.SetActive(true);
    }
}
