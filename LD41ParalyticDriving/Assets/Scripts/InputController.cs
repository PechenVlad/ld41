﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{

    [SerializeField]
    SimpleLookAt HeadDirectionController;
    [SerializeField]
    InputArea CarInputArea;
    [SerializeField]
    InputArea HeadInputArea;

    [Space(20)]
    [SerializeField]
    Transform RightHandHandler;
    [SerializeField]
    RightHand RightHand;

    private void Awake()
    {
        currentInputArea = CarInputArea;
    }

    private void LateUpdate()
    {
        if (!GameController.instance.gameStarts)
            return;

        if (Input.GetKeyDown(KeyCode.Space))
            SwitchInput();
        if (Input.GetMouseButton(0) && HeadDirectionController.LookMirror)
            DoMakeup();

        if (Input.GetMouseButton(0) && !HeadDirectionController.LookMirror)
            RightHand.OnButtonPress();
        if (Input.GetMouseButtonUp(0) && !HeadDirectionController.LookMirror)
            RightHand.OnButtonUp();

        RightHandHandler.position = Vector3.Lerp(RightHandHandler.position, currentInputArea.InputPosition, Time.deltaTime * 2);
    }

    private void SwitchInput()
    {
        HeadDirectionController.SwitchInput();
        if (HeadDirectionController.LookMirror)
            currentInputArea = HeadInputArea;
        else
            currentInputArea = CarInputArea;
    }
    InputArea currentInputArea;
    

    void DoMakeup()
    {
        RightHand.DoMakeup();
    }
}
