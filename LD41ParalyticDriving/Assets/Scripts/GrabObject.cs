﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabObject : MonoBehaviour {

    [SerializeField]
    FacePainter facePainter;
    public FacePainter FacePainter { get { return facePainter; } }

    [SerializeField]
    Outline outline;
    public Outline Outline { get { return outline; } }

    [SerializeField]
    Rigidbody rigidbody;
    public Rigidbody Rigidbody { get { return rigidbody; } }

    [SerializeField]
    Collider collider;
    public Collider Collider { get { return collider; } }


    private void Reset()
    {
        facePainter = GetComponent<FacePainter>();
        outline = GetComponentInChildren<Outline>();
        rigidbody = GetComponentInChildren<Rigidbody>();
        collider = GetComponentInChildren<Collider>();
    }


    public void RemoveRigidbody()
    {
        DestroyImmediate(Rigidbody);
        collider.enabled = false;
    }


    public void AddRigidbody()
    {
        rigidbody = gameObject.AddComponent<Rigidbody>();
        collider.enabled = true;
    }
}
