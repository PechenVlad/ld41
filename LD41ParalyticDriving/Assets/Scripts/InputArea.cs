﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputArea : MonoBehaviour {

    [SerializeField]
    Camera EyeCamera;
    [SerializeField]
    float Width;
    [SerializeField]
    float Height;

    public Vector3 InputPosition { get { return inputPosition; } }

	void Update()
    {
        Vector2 mousePos = Input.mousePosition / EyeCamera.pixelWidth;
        mousePos *= Mathf.Max(Width, Height);
        mousePos.x = Mathf.Clamp(mousePos.x, 0, Width);
        mousePos.y = Mathf.Clamp(mousePos.y, 0, Height);

        inputPosition = transform.TransformPoint(new Vector3(mousePos.x, mousePos.y, 0));
    }
    Vector3 inputPosition;

    private void OnDrawGizmos()
    {
        Vector3 lowerLeft = transform.position;
        Vector3 upperLeft = transform.up * Height + transform.position;
        Vector3 lowerRigth = transform.right * Width + transform.position;
        Vector3 upperRight = transform.up * Height + transform.right * Width + transform.position;
        Debug.DrawLine(lowerLeft, upperLeft);
        Debug.DrawLine(upperLeft, upperRight);
        Debug.DrawLine(upperRight, lowerRigth);
        Debug.DrawLine(lowerRigth, lowerLeft);

        Debug.DrawLine(InputPosition, InputPosition + transform.forward);
    }
}
