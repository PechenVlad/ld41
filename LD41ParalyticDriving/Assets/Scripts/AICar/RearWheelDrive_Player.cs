﻿using UnityEngine;
using System.Collections;

public class RearWheelDrive_Player : MonoBehaviour {

	private WheelCollider[] wheels;

	public float maxAngle = 30;
	public float maxTorque = 300;
	public GameObject wheelShape;
    Rigidbody rigid;
	public GameObject pointCenter;

	public GameObject glass1;
	public GameObject glass2;
	public GameObject glass3;
	private float glassDamage = 0;
	private float onGrassTime = 0;

    [SerializeField]
    Animator HelmAnimator;


    [SerializeField]
    AudioSource AudioSource;
    [SerializeField]
    AudioClip Go;
    [SerializeField]
    AudioClip Idle;


    public bool IsFinished { get { return isFinished; } }
    public bool FallInWater { get { return fallInWater; } }

    // here we find all the WheelColliders down in the hierarchy
    public void Start()
	{
		wheels = GetComponentsInChildren<WheelCollider>();
        rigid = GetComponent<Rigidbody>();

		for (int i = 0; i < wheels.Length; ++i)
		{
			var wheel = wheels [i];

			// create wheel shapes only when needed
			if (wheelShape != null)
			{
				var ws = GameObject.Instantiate (wheelShape);
				ws.transform.parent = wheel.transform;
			}
		}
	}

    // this is a really simple approach to updating wheels
    // here we simulate a rear wheel drive car and assume that the car is perfectly symmetric at local zero
    // this helps us to figure our which wheels are front ones and which are rear
    public void Update()
    {
        if (!GameController.instance.gameStarts)
            return;

        RaycastHit[] hits;
        Ray ray = new Ray(pointCenter.transform.position, new Vector3(0, -1, 0));
        hits = Physics.SphereCastAll(ray, 0.3f, 10.0f);
        //0.4 это радиус сферы, 10.0 это макс дистанция пуляния
        bool onGrass = false;
        foreach (RaycastHit hit in hits)
        {
            string colName = hit.collider.gameObject.name;
            if (colName == "grassLeft" || colName == "grassRight")
                onGrass = true;
        }

        //print("GRASS: " + onGrass);



        float horizontlal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");



        float angle = maxAngle * horizontlal;
        float torque = maxTorque * vertical;

        if (horizontlal > 0.5f)
        {
            HelmAnimator.SetBool("Right", true);
            HelmAnimator.SetBool("Left", false);
        }
        else if (horizontlal < -0.5f)
        {
            HelmAnimator.SetBool("Right", false);
            HelmAnimator.SetBool("Left", true);
        }
        else
        {
            HelmAnimator.SetBool("Right", false);
            HelmAnimator.SetBool("Left", false);
        }

	
		    
        if (Mathf.Abs(vertical) > 0.1f)
        {
	        SwitchClips(Idle, Go, .2f);
        }
        else
        {
	        SwitchClips(Go, Idle, 2f);
        }
        if (!AudioSource.isPlaying)
            AudioSource.Play();

        //if (Input.GetAxis("Vertical") < 0)
        //	torque *= 5;
        //print("MAG: " + rigid.velocity.magnitude);
        if (rigid.velocity.magnitude > 12.0f)
            rigid.velocity = rigid.velocity.normalized * 12;
        if (rigid.velocity.magnitude > 2.0f && onGrass)
            rigid.velocity = rigid.velocity.normalized * 4;
        if (onGrass && rigid.velocity.magnitude > 2.0f && onGrassTime < 60)
		{
			onGrassTime++;
			torque = -300;
		}
		if (onGrass == false)
			onGrassTime = 0;
		

		foreach (WheelCollider wheel in wheels)
		{
			// a simple car where front wheels steer while rear ones drive
			if (wheel.transform.localPosition.z > 0)
				wheel.steerAngle = angle;

			if (wheel.transform.localPosition.z < 0)
				wheel.motorTorque = torque;

			// update visual wheels if any
			if (wheelShape) 
			{
				Quaternion q;
				Vector3 p;
				wheel.GetWorldPose (out p, out q);

				// assume that the only child of the wheelcollider is the wheel shape
				Transform shapeTransform = wheel.transform.GetChild (0);
				shapeTransform.position = p;
				shapeTransform.rotation = q;
			}
		}
	}


    void SwitchClips(AudioClip clipFrom, AudioClip clipTo, float switchToSpeed)
    {
	    if (AudioSource.clip == clipFrom && AudioSource.volume > 0f)
		    AudioSource.volume -= Time.deltaTime * 2f;
	    else
	    {
		    AudioSource.clip = clipTo;
		    if (AudioSource.volume < 0.45f)
			    AudioSource.volume += Time.deltaTime * switchToSpeed;
		    else
			    AudioSource.volume = 0.45f;
	    }
    }
    

	void OnCollisionEnter(Collision collision)
	{
        if (rigid.velocity.magnitude < 10)
            return;
		string name = collision.gameObject.name;
		//print(Time.frameCount + " " + name +  " COLLIDED");

		if (glassDamage == 0)
		{
			glass1.transform.localScale = new Vector3(0,0,0);
			glass2.transform.localScale = new Vector3(0.52f,0.52f,0.52f);
		}
		else if (glassDamage == 1)
		{
			glass2.transform.localScale = new Vector3(0,0,0);
			glass3.transform.localScale = new Vector3(0.52f,0.52f,0.52f);
		}
		else if (glassDamage == 2)
		{
			glass3.AddComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-1,1), Random.Range(-1,1), Random.Range(-1,1)) * 1000);
			//glass3.transform.localScale = new Vector3(0.52f,0.52f,0.52f);
		}

		glassDamage++;

	}


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Finish")
            isFinished = true;

        if (other.tag == "Water")
            fallInWater = true;
    }
    bool isFinished =false;
    bool fallInWater = false;
}