﻿using UnityEngine;
using System.Collections;

public class RearWheelDrive_Auto : MonoBehaviour {

	private WheelCollider[] wheels;

	public float maxAngle = 30;
	public float maxTorque = 300;
	public GameObject wheelShape;
	public string targetLaneName;
	Rigidbody rigid;
	public GameObject pointLeft;
	public GameObject pointRight;
	private float angleSaved;

	// here we find all the WheelColliders down in the hierarchy
	public void Start()
	{
		wheels = GetComponentsInChildren<WheelCollider>();
		rigid = GetComponent<Rigidbody>();

		for (int i = 0; i < wheels.Length; ++i)
		{
			var wheel = wheels [i];

			// create wheel shapes only when needed
			if (wheelShape != null)
			{
				var ws = GameObject.Instantiate (wheelShape);
				ws.transform.parent = wheel.transform;
			}
		}
	}

	// this is a really simple approach to updating wheels
	// here we simulate a rear wheel drive car and assume that the car is perfectly symmetric at local zero
	// this helps us to figure our which wheels are front ones and which are rear
	public void Update()
	{
		RaycastHit[] hits;
		Ray ray = new Ray(pointLeft.transform.position, new Vector3(0, -1, 0));
		hits = Physics.SphereCastAll(ray, 0.1f, 10.0f);
		//0.4 это радиус сферы, 10.0 это макс дистанция пуляния
		bool leftOnLane = false;
		foreach (RaycastHit hit in hits)
		{
			string colName = hit.collider.gameObject.name;
			if (colName == targetLaneName)
				leftOnLane = true;
		}

		ray = new Ray(pointRight.transform.position, new Vector3(0, -1, 0));
		hits = Physics.SphereCastAll(ray, 0.1f, 10.0f);
		//0.4 это радиус сферы, 10.0 это макс дистанция пуляния
		bool rightOnLane = false;
		foreach (RaycastHit hit in hits)
		{
			string colName = hit.collider.gameObject.name;
			if (colName == targetLaneName)
				rightOnLane = true;
		}

		//print(Time.frameCount + " " + leftOnLane + " " + rightOnLane);



		float angle = maxAngle * Input.GetAxis("Horizontal");
		angle = 0;
		float step = 1.0f;
		maxAngle = 10.0f;
		if (leftOnLane == false)
		{
			if (angleSaved <= maxAngle)
				angleSaved += step;
		}
		else if (rightOnLane == false)
		{
			if (angleSaved >= -maxAngle)
				angleSaved -= step;
		}
		else if ((rightOnLane == false && leftOnLane == false) || (rightOnLane == true && leftOnLane == true))
		{
			if (angleSaved > 0)
				angleSaved -= step;
			else if (angleSaved < 0)
				angleSaved += step;
		}
		angle = angleSaved;

		float torque = maxTorque * Input.GetAxis("Vertical");
		torque = 200;
		//print (rigid.velocity.magnitude);
		if (rigid.velocity.magnitude > 3.0f)
			torque = 0;

		foreach (WheelCollider wheel in wheels)
		{
			// a simple car where front wheels steer while rear ones drive
			if (wheel.transform.localPosition.z > 0)
				wheel.steerAngle = angle;

			if (wheel.transform.localPosition.z < 0)
				wheel.motorTorque = torque;

			// update visual wheels if any
			if (wheelShape) 
			{
				Quaternion q;
				Vector3 p;
				wheel.GetWorldPose (out p, out q);

				// assume that the only child of the wheelcollider is the wheel shape
				Transform shapeTransform = wheel.transform.GetChild (0);
				shapeTransform.position = p;
				shapeTransform.rotation = q;
			}

		}
	}
}
