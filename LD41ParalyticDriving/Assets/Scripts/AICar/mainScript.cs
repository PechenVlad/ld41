﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mainScript : MonoBehaviour
{
	public GameObject carForSpawnRight;
	public GameObject carForSpawnLeft;
	public GameObject spawnPointsRight;
	public GameObject spawnPointsLeft;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update ()
	{
		Vector3 posPlayer = GameObject.Find("car_Player").transform.position;

		int rand = Random.Range(0, 50);
		if (rand == 0)
		{
			Transform[] points = spawnPointsRight.GetComponentsInChildren<Transform>();
			int rand2 = Random.Range(0, 2);
			GameObject carForSpawn = carForSpawnRight;
			if (rand2 == 0)
			{
				points = spawnPointsRight.GetComponentsInChildren<Transform>();
				carForSpawn = carForSpawnRight;
			}
			else if (rand2 == 1)
			{
				points = spawnPointsLeft.GetComponentsInChildren<Transform>();
				carForSpawn = carForSpawnLeft;
			}

			//print (points.Length);
			//print(points[0].name);

			bool spawned = false;
			int count = 1;
			for (; count < points.Length; count++)
			{
				Transform point = points[count];
				Vector3 pos = point.position + new Vector3(0, 4.0f, 0);

				float dist = Vector3.Distance(posPlayer, pos);
				if (dist > 50 && dist < 100)
				{
					int rand3 = Random.Range(0, 5);
					if (rand3 == 0)
					{
						spawned = true;
						GameObject newCar = Instantiate(carForSpawn);
						newCar.transform.position = pos;
						Vector3 rot = point.eulerAngles;
						newCar.transform.eulerAngles = rot;
						newCar.GetComponent<Rigidbody>().isKinematic = false;
						Destroy(newCar, 60);
					}
				}
			}
		}
	}
}
