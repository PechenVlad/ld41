﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    [SerializeField]
    float TimeToGo;

    [SerializeField]
    FacePainter LipStick;
    [SerializeField]
    GameObject LipstickToggle;

    [SerializeField]
    FacePainter EyesStick;
    [SerializeField]
    GameObject EyesToggle;

    [SerializeField]
    FacePainter EyebrowStick;
    [SerializeField]
    GameObject EyebrowsToggle;

    [SerializeField, Space(30)]
    RearWheelDrive_Player PlayerCar;
    [SerializeField]
    FadeIn fadeIn;

    [SerializeField]
    Transform TimeArrow;

    public static GameController instance;

    public bool Makeup { get { return LipStick.IsDone && EyesStick.IsDone && EyebrowStick.IsDone; } }
    public bool IsWin { get { return Makeup && !TimeIsLeft && PlayerCar.IsFinished; } }
    public bool TimeIsLeft { get { return TimeToGo < 0f; } }

    public bool gameStarts { get; set; }

    private void Awake()
    {
        initialTime = TimeToGo;
        instance = this;
    }
    float initialTime;

    private void Update()
    {
        if (PlayerCar.FallInWater)
        {
            fadeIn.StartFadeIn("You died young, and without becoming famous.");
            endGame = true;
        }
        if (TimeIsLeft)
        {
            fadeIn.StartFadeIn("Sorry, but you're late. We took another actress.");
            endGame = true;
        }
        if (PlayerCar.IsFinished && !Makeup)
        {
            fadeIn.StartFadeIn("You did not have time to make up on the road. Now you will never become a star.");
            endGame = true;
        }
        if (IsWin)
        {
            fadeIn.StartFadeIn("Congratulations! Casting has already begun, and this is an excuse to make a new level. Vote for us on the ludum gift.");
            endGame = true;
        }


        LipstickToggle.SetActive(LipStick.IsDone);
        EyebrowsToggle.SetActive(EyebrowStick.IsDone);
        EyesToggle.SetActive(EyesStick.IsDone);


        if (Input.GetKeyDown(KeyCode.Return) && gameStarts)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        if (!gameStarts && Input.GetKeyDown(KeyCode.Return))
        {
            fadeIn.StartFadeOut("You are a young girl who dreams of becoming famous and acting in films. " +
                "Now the casting will begin and you need to have time to get there and make up.");
            gameStarts = true;
        }

        if (!TimeIsLeft)
            TimeToGo -= Time.deltaTime;

        TimeArrow.localRotation = Quaternion.Euler(new Vector3(0, -(TimeToGo / initialTime) * 360f, 0));
    }
    bool endGame;


    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
