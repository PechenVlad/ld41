﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacePainter : MonoBehaviour 
{
    [SerializeField]
    Camera MakeupCamera;

    [SerializeField]
    RenderTexture MakeupRT;

    [SerializeField]
    LayerMask Layer;

    [SerializeField]
    Transform BrushParent;

    [SerializeField]
    GameObject Brush;

    [SerializeField]
    Material PaintMaterial;

    [SerializeField]
    float BrushSize = 1f;
    [SerializeField]
    Color BrushColor = Color.red;

    [SerializeField]
    Transform RightHandHandler;
    [SerializeField]
    Transform Head;

    const int MaxBrushCount = 300;

    public bool IsDone { get {return progress > 60; } }


    private void Awake()
    {
        PaintMaterial.mainTexture = Resources.Load("Girl1") as Texture;
    }


    private void Update()
    {
        if (saving)
            return;
        if (Input.GetMouseButtonUp(0))
            initialize = false;
    }


    public void Paint()
    {
        Debug.DrawRay(RightHandHandler.position, Head.forward, Color.blue);

        Vector3 uvWorldPosition = new Vector3(0f, 0f, 0f);
        if (TryHitMesh(ref uvWorldPosition))
        {
            var middleUVPos = Vector3.Lerp(prevUVPosition, uvWorldPosition, 0.5f);

            if (initialize)
                InstantiateBrush(middleUVPos);
            initialize = true;
            InstantiateBrush(uvWorldPosition);
        }
        if (brushesCount > MaxBrushCount)
        {
            BakeTexture();
            brushesCount = 0;
        }
        prevUVPosition = uvWorldPosition;
    }
    int brushesCount;
    Vector3 prevUVPosition;
    bool initialize = false;


    void InstantiateBrush(Vector3 uvPos)
    {
        var brush = Instantiate(Brush);
        brush.transform.position = uvPos;
        brush.transform.SetParent(BrushParent);
        brush.transform.localScale = Vector3.one * BrushSize;

        var spriteRend = brush.GetComponent<SpriteRenderer>();
        spriteRend.color = BrushColor;

        brushesCount++;
    }


    private void BakeTexture()
    {
        saving = true;
        RenderTexture.active = MakeupRT;
        int width = MakeupRT.width;
        int height = MakeupRT.height;
        Texture2D tex = new Texture2D(width, height, TextureFormat.ARGB32, false);
        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tex.Apply();
        RenderTexture.active = null;
        PaintMaterial.mainTexture = tex;
        foreach (Transform brush in BrushParent.transform)
            Destroy(brush.gameObject);
        saving = false;
    }
    bool saving;


    bool TryHitMesh(ref Vector3 uvWorldPosition)
    {
        RaycastHit hit;
        var ray = new Ray(RightHandHandler.position, -Head.forward);
        if (Physics.Raycast(ray, out hit, Layer))
        {
            var hitCoord = new Vector3(hit.textureCoord.x, hit.textureCoord.y);
            uvWorldPosition.x = (hitCoord.x - 0.5f) * MakeupCamera.orthographicSize * 2f;
            uvWorldPosition.y = (hitCoord.y - 0.5f) * MakeupCamera.orthographicSize * 2f;
            uvWorldPosition.z = 9f;
                progress++;
            return true;
        }
        else
            return false;
    }
    int progress;
}
