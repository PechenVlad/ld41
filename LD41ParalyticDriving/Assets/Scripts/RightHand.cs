﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightHand : MonoBehaviour {

    [SerializeField]
    float OverlapRadius = 1;
    [SerializeField]
    Vector3 InitialRotation = new Vector3(0, -2.35f, 0);
    [SerializeField]
    Transform OverlapPointTransform;
    [SerializeField]
    Transform ObjectsParent;
    [SerializeField]
    Camera HeadCamera;
    [SerializeField]
    LayerMask Objectsmask;

    public GrabObject ObjectInHand { get { return objectInHand; } }


    void Awake()
    {
        StartCoroutine(FUpdate());
    }


    public void OnButtonPress()
    {
        if (objectInHand != null)
        {
            ThrowOut();
            return;
        }

        Collider[] colliders = Physics.OverlapSphere(OverlapPointTransform.position, OverlapRadius);

        if (objectsToGrab != null)
            objectsToGrab.ForEach(o => o.Outline.OutlineWidth = 0);


        //
        Ray ray = HeadCamera.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, HeadCamera.nearClipPlane));
        var hits = Physics.RaycastAll(ray, 100, Objectsmask);

        if (hits.Length > 0 && hits[0].transform.GetComponent<GrabObject>())
        {
            stuffForUp = hits[0].transform.GetComponent<GrabObject>(); ;

            if (stuffForUp != null)
            {
                stuffForUp.Outline.OutlineWidth = 10;
            }
            return;
        }
        //



        objectsToGrab = new List<GrabObject>();
        foreach (var col in colliders)
        {
            var grabObj = col.GetComponentInParent<GrabObject>();
            if (grabObj != null)
                objectsToGrab.Add(grabObj);
        }

        float minDist = float.MaxValue;
        GrabObject stuff = null;
        for (int i = 0; i < objectsToGrab.Count; i++)
        {
            var dist = (OverlapPointTransform.position - objectsToGrab[i].transform.position).sqrMagnitude;
            if (dist < minDist)
            {
                minDist = dist;
                stuff = objectsToGrab[i];
            }
        }

        objectsToGrab.ForEach(o => o.Outline.OutlineWidth = 0);

        if (stuff != null)
        {
            stuff.Outline.OutlineWidth = 10;
        }
        stuffForUp = stuff;
    }
    GrabObject stuffForUp;
    List<GrabObject> objectsToGrab;


    public void OnButtonUp()
    {
        if (objectsToGrab != null)
            objectsToGrab.ForEach(o => o.Outline.OutlineWidth = 0);
        if (stuffForUp == null || throwOut)
        {
            throwOut = false;
            return;
        }

        objectInHand = stuffForUp;
        shouldGrab = true;
    }
    GrabObject objectInHand;


    public void ThrowOut()
    {
        if (objectInHand == null)
            return;

        throwOut = true;
        objectInHand.AddRigidbody();
        objectInHand.transform.SetParent(ObjectsParent);

        objectInHand = null;
    }
    bool throwOut = false;


    public void DoMakeup()
    {
        if (objectInHand == null || objectInHand.FacePainter == null)
            return;

        objectInHand.FacePainter.Paint();
    }


    private IEnumerator FUpdate()
    {
        while (true)
        {
            if (objectInHand != null && shouldGrab)
            {
                shouldGrab = false;
                objectInHand.RemoveRigidbody();
                objectInHand.transform.SetParent(this.transform);
                objectInHand.transform.localRotation = Quaternion.identity;
                objectInHand.transform.localPosition = Vector3.zero;
            }

            yield return new WaitForEndOfFrame();
        }
    }
    bool shouldGrab;
}
