﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleLookAt : MonoBehaviour {

    [SerializeField]
    Transform TargetTransform;
    [SerializeField]
    Transform SecondTargetTransform;

    public bool LookMirror { get { return lookSecondTarget; } }


    private void Update()
    {
        Vector3 forward;
        if (!LookMirror)
            forward = TargetTransform.position - transform.position;
        else
            forward = SecondTargetTransform.position - transform.position;

        transform.rotation = Quaternion.LookRotation(forward, SecondTargetTransform.up);
    }

    public void SwitchInput()
    {
        lookSecondTarget = !LookMirror;
        CameraController.instace.ZoomIn(!LookMirror);
    }
    bool lookSecondTarget = false;
}
