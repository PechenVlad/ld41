﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField]
    Camera camera;

    public static CameraController instace;


    private void Awake()
    {
        instace = this;
        zoomIn = true;
    }


    private void Reset()
    {
        camera = GetComponent<Camera>();
    }


    private void Update()
    {
        if (zoomIn)
            sign = -1;
        else
            sign = 1;

        t += Time.deltaTime * sign;
        t = Mathf.Clamp01(t);
        camera.fieldOfView = Mathf.SmoothStep(100, 60, t);
    }
    float t;
    int sign;


    public void ZoomIn(bool zoomIn)
    {
        this.zoomIn = zoomIn;
    }
    bool zoomIn;
}
